<?php

namespace Seko\QueueBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SekoQueueExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        foreach ($configs as $ind => $subConfig) {
            !isset($subConfig['queue_names']) or $container->setParameter(
                Configuration::ROOT_NAME.'.queue_names',
                $subConfig['queue_names']
            );

            // Get array with keys: seko_queue.services.Redis, seko_queue.services.RedisTest and etc. 
            $config = $this->transformToPhrasesWithDots($subConfig, null, null, 2);
            foreach ($config as $key => $value) {
                strpos($key, 'queue_names') !== false or $container->setParameter(
                    Configuration::ROOT_NAME.'.'.$key,
                    $value
                );
            }
        }

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @param array $elem
     * @param null $path
     * @param null $result
     * @param int $maxDepth
     * @return array|null
     */
    protected function transformToPhrasesWithDots($elem, $path = null, $result = null, $maxDepth = 10)
    {
        null !== $result or $result = [];
        if (is_array($elem) && $maxDepth) {
            foreach ($elem as $key => $value) {
                $newPath = $path ? $path . '.' . $key : $key;
                $result = $this->transformToPhrasesWithDots($value, $newPath, $result, $maxDepth - 1);
            }
        } else {
            $result[$path] = $elem;
        }

        return $result;
    }
}
