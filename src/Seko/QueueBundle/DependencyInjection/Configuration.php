<?php

namespace Seko\QueueBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    const ROOT_NAME = 'seko_queue';
    
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(static::ROOT_NAME);

        $configType = 'type';
        $serviceRedis = 'redis';
        $serviceSync = 'synchronized';

        $rootNode
            ->children()
                ->arrayNode('services')
                ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->append($this->addEnabledNode(false))
                            ->scalarNode($configType)
                                ->isRequired()
                                ->validate()
                                ->ifNotInArray([
                                    $serviceRedis,
                                    $serviceSync,
                                ])->thenInvalid('Invalid service type: %s')
                                ->end()
                            ->end()
                            ->arrayNode('servers')
                                ->children()
                                    ->scalarNode('host')->defaultNull()->end()
                                    ->scalarNode('port')->defaultNull()->end()
                                    ->scalarNode('database')->defaultNull()->end()
                                ->end()
                            ->end()
                        ->end()
                        ->addDefaultsIfNotSet()
                    ->end()
                ->end()
                ->arrayNode('queue_names')
                    ->performNoDeepMerging()
                    ->prototype('scalar')->end()
                ->end()
            ->end();
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }

    /**
     * @param bool $default
     *
     * @return NodeDefinition
     */
    public function addEnabledNode($default = FALSE)
    {
        $builder = new TreeBuilder();
        $node = $builder->root('enabled', 'boolean')->defaultValue($default);

        return $node;
    }
}
