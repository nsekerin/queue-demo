<?php

namespace Seko\QueueBundle\Controller;

use Seko\QueueBundle\Queue\Manager;
use Seko\QueueBundle\Queue\Service;
use Seko\QueueBundle\Queue\Worker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    const SERVICE_NAME = Service::SERVICE_INSTANCE_REDIS;
    const QUEUE_NAME = 'call_center';

    public function indexAction()
    {
        /** @var Manager $queueManager */
        $queueManager = $this->get('seko.queue.manager');

        $queueManager->pushQueue(
            self::SERVICE_NAME,
            self::QUEUE_NAME,
            [
                'worker' => Worker\Manager::WORKER_CALL_CENTER,
                'data' => [
                    'host' => 'test.ru',
                ],
            ]
        );

        $queueManager->pushQueue(
            self::SERVICE_NAME,
            self::QUEUE_NAME,
            [
                'worker' => Worker\Manager::WORKER_CALL_CENTER,
                'data' => [
                    'host' => 'test1.ru',
                ],
            ]
        );

        $queueManager->pushQueue(
            self::SERVICE_NAME,
            self::QUEUE_NAME,
            [
                'worker' => Worker\Manager::WORKER_CALL_CENTER,
                'data' => [
                    'host' => 'test2.ru',
                ],
            ]
        );

        return $this->render('SekoQueueBundle:Default:index.html.twig');
    }
}
