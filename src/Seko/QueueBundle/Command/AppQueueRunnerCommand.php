<?php

namespace Seko\QueueBundle\Command;

use Seko\QueueBundle\Exception\NoJobFoundException;
use Seko\QueueBundle\Queue\Job;
use Seko\QueueBundle\Queue\Manager;
use Seko\QueueBundle\Queue\Worker;
use Seko\QueueBundle\Queue\Service;
use Seko\QueueBundle\Exception\WorkerNotFoundException;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppQueueRunnerCommand extends ContainerAwareCommand
{
    const RUN_SLEEP = 25;

    /** @var bool */
    protected $isDelayed = false;
    /** @var null|string  */
    protected $queueName = null;
    /** @var null|string */
    protected $serviceName = null;
    /** @var null|Service */
    protected $service = null;

    /** @var OutputInterface */
    protected $output;

    protected function configure()
    {
        $this
            ->setName('app:queue:runner')
            ->setDescription('Queue runner')
            ->addArgument(
                'service-name',
                InputArgument::REQUIRED,
                'Service name'
            )
            ->addArgument(
                'queue-name',
                InputArgument::REQUIRED,
                'Queue name'
            );
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->queueName = $input->getArgument('queue-name');
        $this->serviceName = $input->getArgument('service-name');

        $this->beforeLoop();
        $this->loop();
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function beforeLoop()
    {
        if (!$this->queueName) {
            throw new \Exception('Queue name is invalid.');
        }
        $this->writeLn('QueueName: ' . $this->queueName);

        /** @var Manager $queueManager */
        $queueManager = $this->getContainer()->get('seko.queue.manager');

        $this->service = $this->serviceName
            ? $queueManager->getService($this->serviceName)
            : $queueManager->getDefaultService();
    }

    /**
     * @return void
     */
    protected function loop()
    {
        while (true) {
            $this->workJob();
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function workJob()
    {
        try {
            $job = $this->service->getJob($this->queueName);

            if (!$job) {
                throw new NoJobFoundException();
            }

            if (!($job instanceof Job)) {
                throw new \Exception('Wrong job type.');
            }

            if (!isset($job->worker) || !$job->worker) {
                print_r($job);
                throw new \Exception('No worker found.');
            }

            // starting workerJob
            $this->isDelayed = false;
            $this->processWorker($job->worker, $job);
        } catch (NoJobFoundException $e) {
            if (!$this->isDelayed) {
                $this->isDelayed = true;
                $this->writeLn('Waiting jobs');
            }
            sleep(self::RUN_SLEEP);
        } catch (\Exception $e) {
            if (isset($job) && $job) {
                $this->writeLn(
                    sprintf(
                        '%s ! Job [ %s ] - error: %s',
                        $job->queueName,
                        $job->worker,
                        $e->getMessage()
                    )
                );
            }
        }
    }

    /**
     * @param $workerName
     * @param $job
     * @throws WorkerNotFoundException
     */
    protected function processWorker($workerName, $job)
    {
        // processing
        $this->writeLn(
            sprintf(
                '%s * Job [ %s ] - started',
                $job->queueName,
                $workerName
            )
        );

        $mt = microtime(true);
        $worker = Worker\Manager::getWorker($workerName);
        $worker->runJob($job);

        $this->writeLn(
            sprintf(
                '%s * Job [ %s ] - finished [ %.6f sec ]',
                $job->queueName,
                $workerName,
                microtime(true) - $mt
            )
        );
    }

    /**
     * @param $message
     */
    protected function writeLn($message)
    {
        $this->output->write($message, true);
    }
}
