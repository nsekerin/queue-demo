<?php

namespace Seko\QueueBundle\Queue\Worker;

use Seko\QueueBundle\Queue\Job;

class MailerWorker implements WorkerInterface
{
    /**
     * @param Job $jobObject
     */
    public function runJob($jobObject)
    {
        $jobData = isset($jobObject->data) ? $jobObject->data : null;

        if (!isset($jobData, $jobData['emailTo'])) {
            throw new \InvalidArgumentException('emailTo is not defined.');
        }

        // TODO: send message on email
    }
}