<?php

namespace Seko\QueueBundle\Queue\Worker;

use Seko\QueueBundle\Exception\WorkerNotFoundException;

class Manager
{
    /** @var WorkerInterface[] */
    private static $allWorkers = [];

    const WORKER_CALL_CENTER = 'CallCenterWorker';
    const WORKER_MAILER = 'MailerWorker';

    /**
     * @param $workerName
     * @return WorkerInterface
     * @throws WorkerNotFoundException
     */
    public static function getWorker($workerName)
    {
        if (!$workerName) {
            throw new \InvalidArgumentException('Worker name is empty');
        }

        $worker = isset(self::$allWorkers[$workerName]) ? self::$allWorkers[$workerName] : null;

        if (!$worker && !($worker instanceof WorkerInterface)) {
            $className = self::getWorkerClassName($workerName);
            try {
                self::$allWorkers[$workerName] = new $className();
            } catch (\Exception $e) {
                throw new WorkerNotFoundException($e->getMessage(), $e->getCode());
            }
        }

        return self::$allWorkers[$workerName];
    }

    /**
     * @param  string $workerName
     * @return string
     * @throws WorkerNotFoundException
     */
    protected static function getWorkerClassName($workerName)
    {
        $fullClassName = __NAMESPACE__ . "\\" . $workerName;
        if (!class_exists($fullClassName, true)) {
            throw new WorkerNotFoundException("Worker file/class does not exist: $workerName");
        }

        return $fullClassName;
    }
}