<?php

namespace Seko\QueueBundle\Queue\Worker;

use Seko\QueueBundle\Queue\Job;

class CallCenterWorker implements WorkerInterface
{
    /**
     * @param Job $jobObject
     */
    public function runJob($jobObject)
    {
        $jobData = isset($jobObject->data) ? $jobObject->data : null;

        if (!isset($jobData, $jobData['host'])) {
            throw new \InvalidArgumentException('host is not defined.');
        }

        // TODO: process something and send to a call center
        sleep(5);
    }
}