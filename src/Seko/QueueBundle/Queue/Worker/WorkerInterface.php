<?php

namespace Seko\QueueBundle\Queue\Worker;

use Seko\QueueBundle\Queue\Job;

interface WorkerInterface
{
    /**
     * @param Job $jobObject
     */
    public function runJob($jobObject);
}