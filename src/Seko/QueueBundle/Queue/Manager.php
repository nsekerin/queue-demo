<?php

namespace Seko\QueueBundle\Queue;

class Manager
{
    /** @var null|ServiceManager */
    protected $sm = null;

    /**
     * Manager constructor.
     * @param ServiceManager $sm
     */
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param $serviceName
     * @param $queueName
     * @param $queueData
     * @throws \Exception
     */
    public function pushQueue($serviceName, $queueName, $queueData)
    {
        $serviceInstance = $this->sm->getService($serviceName);
        $serviceInstance->pushJob($queueData, $queueName);
    }

    /**
     * @param $serviceName
     * @return Service
     * @throws \Exception
     */
    public function getService($serviceName)
    {
        return $this->sm->getService($serviceName);
    }

    /**
     * @return Service
     * @throws \Exception
     */
    public function getDefaultService()
    {
        return $this->sm->getService(Service::SERVICE_INSTANCE_DEFAULT);
    }
}