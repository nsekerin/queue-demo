<?php

namespace Seko\QueueBundle\Queue;

class Job
{
    const OK = 'success';
    const NOT_OK = 'failed';

    public $data;
    public $worker;
    public $status = self::OK;
    public $queueName;

    /**
     * Job constructor.
     * @param $data
     * @param $queueName
     */
    public function __construct($data, $queueName)
    {
        if ($data) {
            if (is_array($data)) {
                $this->worker = $data['worker'];
                $this->data = $data['data'];
            } elseif (is_object($data)) {
                $this->worker = $data->worker;
                $this->data = $data->data;
            } else {
                try {
                    $data = json_decode($data, true);
                    $this->worker = $data['worker'];
                    $this->data = $data['data'];
                } catch (\Exception $e) {}
            }
        }

        !$queueName or $this->queueName = $queueName;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return ($this->status === self::OK);
    }

    public function onSuccessful()
    {
        $this->status = self::OK;
    }

    public function onError()
    {
        $this->status = self::NOT_OK;
    }
}