<?php

namespace Seko\QueueBundle\Queue;

class ServiceManager
{
    const CONFIG_ENABLED = 'enabled';
    const CONFIG_TYPE = 'type';

    /** @var Service[] */
    protected $services = [];

    /** @var null|array */
    protected $instanceRedisConfig = null;
    /** @var null|array */
    protected $instanceRedisTestConfig = null;
    /** @var null|array */
    protected $instanceSyncConfig = null;

    /**
     * ServiceManager constructor.
     * @param $redisConfig
     * @param $redisTestConfig
     * @param $syncConfig
     */
    public function __construct($redisConfig, $redisTestConfig, $syncConfig)
    {
        $this->instanceRedisConfig = $redisConfig;
        $this->instanceRedisTestConfig = $redisTestConfig;
        $this->instanceSyncConfig = $syncConfig;
    }

    /**
     * @param $serviceName
     * @return Service
     * @throws \InvalidArgumentException
     */
    public function getService($serviceName)
    {
        $serviceName = trim($serviceName);
        $serviceName or $serviceName = Service::SERVICE_INSTANCE_DEFAULT;

        $configService = $this->getServiceConfig($serviceName);
        if (!isset($configService[self::CONFIG_ENABLED]) || !$configService[self::CONFIG_ENABLED]) {
            throw new \InvalidArgumentException(sprintf('Service instance "%s" is not enabled', $serviceName));
        }

        $serviceType = isset($configService[self::CONFIG_TYPE]) ? $configService[self::CONFIG_TYPE] : null;
        if (!$serviceType) {
            throw new \InvalidArgumentException('Service type is not defined');
        }

        if (isset($this->services[$serviceName]) && $this->services[$serviceName] instanceof Service) {
            $service = $this->services[$serviceName];
        } else {
            $service = new Service($serviceType, isset($configService['servers']) ? $configService['servers'] : null);
        }

        $this->services[$serviceName] = $service;

        return $service;
    }

    /**
     * @param string $serviceName
     * @return array|null
     */
    public function getServiceConfig($serviceName)
    {
        $config = [];
        switch ($serviceName) {
            case Service::SERVICE_INSTANCE_REDIS:
                $config = $this->instanceRedisConfig;
                break;
            case Service::SERVICE_INSTANCE_REDIS_TEST:
                $config = $this->instanceRedisTestConfig;
                break;
            case Service::SERVICE_INSTANCE_SYNC:
                $config = $this->instanceSyncConfig;
                break;
        }

        return $config;
    }
}