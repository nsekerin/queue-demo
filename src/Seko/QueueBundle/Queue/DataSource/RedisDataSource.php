<?php

namespace Seko\QueueBundle\Queue\DataSource;

use \Symfony\Component\OptionsResolver\OptionsResolver;
use \Predis\Client as Predis;

class RedisDataSource implements DataSourceInterface
{
    const TYPE_STRING = 'string';
    const TYPE_HASH = 'hash';
    const TYPE_LIST = 'list';
    const TYPE_SET = 'set';
    const TYPE_NONE = 'none';

    /** @var null|Predis */
    protected $connection = null;

    /**
     * RedisDataSource constructor.
     * @param array $parameters
     */
    public function __construct($parameters = [])
    {
        $parameters = (new OptionsResolver())->setDefaults(
            [
                'scheme' => 'tcp',
                'host' => '127.0.0.1',
                'port' => 6379,
                'database' => 15,
            ]
        )->resolve($parameters);

        $this->connection = new Predis($parameters);
    }

    /**
     * @return null|Predis
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param $data
     * @param $key
     */
    public function push($data, $key)
    {
        $encodedData = json_encode($data);

        $this->getConnection()->rpush($key, $encodedData);
    }

    /**
     * @param $key
     * @return bool
     */
    public function keyExists($key)
    {
        return (bool) $this->getConnection()->exists($key);
    }

    /**
     * @param null $key
     * @return array|mixed|null|string
     * @throws \Exception
     */
    public function get($key = null)
    {
        if (!$this->keyExists($key)) {
            return null;
        }

        $type = (string) $this->getConnection()->type($key);
        switch ($type) {
            case self::TYPE_LIST:
                $data = $this->getConnection()->lpop($key);
                !$data or $data = json_decode($data, true);
                break;
            case self::TYPE_STRING:
                $data = $this->getConnection()->get($key);
                break;
            case self::TYPE_HASH:
                if (func_num_args() > 2) {
                    $field = func_get_arg(2);
                    $data = $this->getConnection()->hmget($key, [$field]);
                } else {
                    $data = $this->getConnection()->hgetall($key);
                }
                break;
            case self::TYPE_NONE:
                return null;
            default:
                throw new \Exception(sprintf('Data type (%s) not supported yet.', $type));
                break;
        }

        return $data;
    }
}