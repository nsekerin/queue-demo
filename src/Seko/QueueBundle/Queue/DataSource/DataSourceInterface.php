<?php

namespace Seko\QueueBundle\Queue\DataSource;

interface DataSourceInterface 
{
    public function push($data, $key);
    public function get($key);
}