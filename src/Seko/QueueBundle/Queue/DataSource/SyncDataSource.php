<?php

namespace Seko\QueueBundle\Queue\DataSource;

use Seko\QueueBundle\Queue\Job;
use Seko\QueueBundle\Queue\Worker\Manager;
use Seko\QueueBundle\Exception\WorkerNotFoundException;

class SyncDataSource implements DataSourceInterface
{
    protected $parameters = null;

    /**
     * SyncDataSource constructor.
     * @param $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param $data
     * @param $key
     * @throws WorkerNotFoundException
     */
    public function push($data, $key)
    {
        if (!$data) {
            throw new \InvalidArgumentException('No data specified.');
        }
        
        $newJob = new Job($data, null);
        if (!isset($newJob->worker) || !$newJob->worker) {
            throw new \InvalidArgumentException('No worker found.');
        }

        // Process immediately
        if (is_string($newJob->worker)) {
            $worker = Manager::getWorker($newJob->worker);
            $worker->runJob($newJob);
        }
    }

    /**
     * @param $key
     * @return null
     */
    public function get($key)
    {
        return null;
    }
}