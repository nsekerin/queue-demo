<?php

namespace Seko\QueueBundle\Queue;

use Seko\QueueBundle\Queue\DataSource\DataSourceInterface;
use Seko\QueueBundle\Queue\DataSource\RedisDataSource;
use Seko\QueueBundle\Queue\DataSource\SyncDataSource;

class Service
{
    const SERVICE_BEANSTALKD = 'beanstalkd';
    const SERVICE_REDIS = 'redis';
    const SERVICE_SYNC = 'synchronized';

    const SERVICE_INSTANCE_REDIS = 'Redis';
    const SERVICE_INSTANCE_REDIS_TEST = 'RedisTest';
    const SERVICE_INSTANCE_BEANSTALKD = 'Beanstalkd';
    const SERVICE_INSTANCE_SYNC = 'Sync';

    // Default service
    const SERVICE_DEFAULT = self::SERVICE_REDIS;
    // Default service instance
    const SERVICE_INSTANCE_DEFAULT = self::SERVICE_INSTANCE_REDIS;

    /** @var string|null */
    protected $serviceType = null;
    
    /** @var DataSourceInterface|null */
    protected $dataSource = null;

    /**
     * Service constructor.
     * @param string $serviceType
     * @param array $parameters
     */
    public function __construct($serviceType, $parameters = [])
    {
        $this->serviceType = $serviceType;
        
        switch ($serviceType) {
            case self::SERVICE_REDIS:
                $this->dataSource = new RedisDataSource($parameters);
                break;
            default:
                $this->dataSource = new SyncDataSource($parameters);
        }
    }

    /**
     * @param array $data
     * @param string $key
     */
    public function pushJob($data, $key)
    {
        $newJob = new Job($data, $key);
        $this->dataSource->push($newJob, $key);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getJob($key)
    {
        $data = $this->dataSource->get($key);
        return $data ? (new Job($data, $key)) : null;
    }

    /**
     * @return null|string
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @return DataSourceInterface
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }
}