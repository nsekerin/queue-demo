<?php

namespace Seko\QueueBundle\Tests\Queue;

use Seko\QueueBundle\Queue\Job;
use Seko\QueueBundle\Queue\Worker\Manager;

class JobTest extends \PHPUnit_Framework_TestCase
{
    public function testJobCreate()
    {
        $newJob = new Job(
            ['worker' => Manager::WORKER_CALL_CENTER, 'data' => ['host' => 'test.ru']], 'call_center'
        );
        $this->assertTrue($newJob->worker === Manager::WORKER_CALL_CENTER);
        $this->assertTrue($newJob->data === ['host' => 'test.ru']);
        $this->assertTrue($newJob->queueName === 'call_center');
        $this->assertTrue($newJob->isSuccessful());
    }
}