<?php

namespace Seko\QueueBundle\Tests\Queue\DataSource;

use Seko\QueueBundle\Queue\DataSource\RedisDataSource;

class RedisDataSourceTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateRedisDataSourcePushAndGet()
    {
        $queueName = 'some_queue2';
        $data = ['test'];
        
        $rds = (new RedisDataSource());
        $rds->push($data, $queueName);
        $this->assertTrue($rds->keyExists($queueName));

        $data2 = $rds->get($queueName);
        $this->assertEquals($data, $data2);
        $this->assertFalse($rds->keyExists($queueName));
    }
}