<?php

namespace Seko\QueueBundle\Tests\Queue\Worker;

use Seko\QueueBundle\Queue\Worker\CallCenterWorker;
use Seko\QueueBundle\Queue\Worker\MailerWorker;
use Seko\QueueBundle\Queue\Worker\Manager;

class ManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testWorkerCreateEmptyWorkerName()
    {
        $worker = Manager::getWorker('');
    }

    /**
     * @expectedException \Seko\QueueBundle\Exception\WorkerNotFoundException
     */
    public function testWorkerCreateNotValidWorkerName()
    {
        $worker = Manager::getWorker('test');
    }

    public function testWorkerCreate()
    {
        $worker1 = Manager::getWorker(Manager::WORKER_CALL_CENTER);
        $worker2 = Manager::getWorker(Manager::WORKER_MAILER);
        $this->assertTrue($worker1 instanceof CallCenterWorker);
        $this->assertTrue($worker2 instanceof MailerWorker);
    }
}