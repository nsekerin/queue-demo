<?php

namespace Seko\QueueBundle\Tests\Queue;

use Seko\QueueBundle\Queue\DataSource\RedisDataSource;
use Seko\QueueBundle\Queue\DataSource\SyncDataSource;
use Seko\QueueBundle\Queue\Job;
use Seko\QueueBundle\Queue\Service;
use Seko\QueueBundle\Queue\Worker;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testServiceCreatePushJobAndGetJob()
    {
        $service1 = (new Service(Service::SERVICE_REDIS));

        $queueName = 'some_queue1';
        $service1->pushJob(
            ['worker' => Worker\Manager::WORKER_CALL_CENTER, 'data' => ['host' => 'test.ru']],
            $queueName
        );
        $job = $service1->getJob($queueName);
        
        $this->assertEquals($service1->getServiceType(), Service::SERVICE_REDIS);
        $this->assertTrue($service1->getDataSource() instanceof RedisDataSource);        
        $this->assertTrue($job instanceof Job);
        $this->assertEquals($job->worker, Worker\Manager::WORKER_CALL_CENTER);
        $this->assertEquals($job->data, ['host' => 'test.ru']);
        $this->assertEquals($job->queueName, $queueName);

        $service2 = (new Service(Service::SERVICE_SYNC));
        
        $this->assertEquals($service2->getServiceType(), Service::SERVICE_SYNC);
        $this->assertTrue($service2->getDataSource() instanceof SyncDataSource);
    }
}