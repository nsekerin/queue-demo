<?php

namespace Seko\QueueBundle\Tests\Queue;

use Seko\QueueBundle\Queue\Service;
use Seko\QueueBundle\Queue\ServiceManager;
use Seko\QueueBundle\Queue\Worker;

class ServiceManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testGetService()
    {
        $sm = new ServiceManager(
            ['enabled' => true, 'type' => Service::SERVICE_REDIS, 'servers' => []], [], []
        );
        
        $service = $sm->getService(Service::SERVICE_INSTANCE_REDIS);
        $this->assertTrue($service instanceof Service);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetNotEnabledService()
    {
        $sm = new ServiceManager(
            ['enabled' => false, 'type' => Service::SERVICE_REDIS, 'servers' => []], [], []
        );

        $service = $sm->getService(Service::SERVICE_INSTANCE_REDIS);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetServiceWithNoTypeDefined()
    {
        $sm = new ServiceManager(
            ['enabled' => true, 'servers' => []], [], []
        );

        $service = $sm->getService(Service::SERVICE_INSTANCE_REDIS);
    }
    
    public function testGetServiceConfig()
    {
        $redisConfig = ['enabled' => true, 'type' => Service::SERVICE_REDIS, 'servers' => []];
        $sm = new ServiceManager(
            $redisConfig, [], []
        );
        
        $this->assertEquals($sm->getServiceConfig(Service::SERVICE_INSTANCE_REDIS), $redisConfig);
    }
}