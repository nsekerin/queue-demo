Simple queueing system.
==========

Redis and immediate processing realization (Sync).

    1. Push jobs  (look at \Seko\QueueBundle\Controller\DefaultController.php)
    2. Process jobs inside queue (look at \Seko\QueueBundle\Command\AppQueueRunnerCommand.php)
    3. Unit tests (look at \Seko\QueueBundle\Tests\...)